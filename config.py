# Configuration

# KonoCloud
KONOCLOUD_API_URI = "http://worker2:3000/api"
KONOCLOUD_API_USER_GROUP = "cn=User,ou=groups,dc=konodrac,dc=com"
KONOCLOUD_API_USER_GROUP_ANALYST = "cn=Analyst,ou=groups,dc=konodrac,dc=com"
KONOCLOUD_API_USER_ID = "admin"

# Twitter API params
TWITTER_API_KEY = "WakvivD5wuYvED6852F3xtTIf"
TWITTER_API_SECRET = "2rIfbQmddRJySUA4owkdfTuETNeiK4N2RabDdAeJHfkBhdEFbj"

# Access tokens
#TWITTER_ACCESS_TOKEN_KEY = "<introduce access token key>"
#TWITTER_ACCESS_TOKEN_SECRET = "<introduce access token secret>"

# Prod
# TWITTER_API_KEY = "WakvivD5wuYvED6852F3xtTIf"
# TWITTER_API_SECRET = "2rIfbQmddRJySUA4owkdfTuETNeiK4N2RabDdAeJHfkBhdEFbj"

# Dev
TWITTER_API_KEY = "DoSbjGZ0Bp1eGKLdEFt2trcxP"
TWITTER_API_SECRET = "feaEpxkqwhkyCq5EsC4FxTRcvGQHRql8ZW34bIPQmTclEGqiif"

# KonodracDev prod
# TWITTER_ACCESS_TOKEN_KEY = "2510638410-e5pG6hO35YvhJkrO7RGTfQak6EwV84H2PiCsv6y"
# TWITTER_ACCESS_TOKEN_SECRET = "OVR38ZDSPkTCkKZI35VxU9tYn4gSUlt6z9jZOjioUPzAt"

# KonodracDev dev
TWITTER_ACCESS_TOKEN_KEY = "2510638410-nEkTtScfGFCtiHQQwl2jlA7uHVbJ3pG4Harm3sG"
TWITTER_ACCESS_TOKEN_SECRET = "2eelqPbHFiIhFU6axCGrguCsRNTKL9tWR2cYhpmlQtATw"

# Mongo params
MONGO_HOST = "worker3"
MONGO_PORT = 27017

# Zookeeper params
ZOOKEEPER_HOSTS = "worker4.konodrac.local:2181,worker2.konodrac.local:2181,worker3.konodrac.local:2181"

# Kafka
KAFKA_URI = 'worker1:6667,worker2:6667'
#KAFKA_TIMEOUT = 60

# Slack
# Slack
SLACK_TOKEN = "xoxp-18390195648-18396570839-82306411701-9e181dd089d2a0ba9fac7c1ad5265515"
#SLACK_TOKEN = "xoxp-323481626258-322929049297-335402142592-df54905f8aaf24114897d4ec877f8f6b"
SLACK_TWITTERSTREAM_USER = 'twitter-source twitterstream'
SLACK_TWITTERANALYTICSCOLLECTOR_USER = 'twitter-source twitter-analytics-collector'

# Twitter Analytics
TWITTERANALYTICS_BASE_DATA_PATH = "/mnt/nfs/aflow_inbox"
TWITTERANALYTICS_NUMERIC_FIELDS = ["impressions","engagements",
            "engagement_rate","retweets","replies","likes","user_profile_clicks","url_clicks",
            "hashtag_clicks","detail_expands","permalink_clicks","app_opens","app_installs",
            "follows","email_tweet","dial_phone","media_views","media_engagements",
            "promoted_impressions","promoted_engagements","promoted_engagement rate",
            "promoted_retweets","promoted_replies","promoted_likes","promoted_user profile clicks",
            "promoted_url clicks","promoted_hashtag clicks","promoted_detail expands",
            "promoted_permalink clicks","promoted_app opens","promoted_app installs",
            "promoted_follows","promoted_email tweet","promoted_dial phone","promoted_media views",
            "promoted_media engagements"]

# Elasticsearch
ELASTICSEARCH_URI = "http://worker4.konodrac.local:9200"
ELASTICSEARCH_BULK_SIZE = 1000
ELASTICSEARCH_TIMEOUT = 30
ELASTICSEARCH_BULK_RETRIES = 10
